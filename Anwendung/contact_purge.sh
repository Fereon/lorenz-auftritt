#!/bin/bash

sudo docker-compose -f /home/lorenz/lorenz-auftritt/Anwendung/docker-compose.prod.yml exec app python manage.py shell -c "from contact.models import Contact; Contact.objects.privacy_purge()"
