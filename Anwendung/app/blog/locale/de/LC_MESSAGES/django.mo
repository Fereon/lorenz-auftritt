��          |      �          8   !     Z  )   _     �  
   �  "   �     �     �  	   �     �     �  B  �  G   )     q  )   v     �     �  6   �     �     �     �                             
      	                                  A short description of the image used as alternate text. Blog Please enter an article body in markdown. blog entries blog entry human readable name of this image. image images published slug title Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Eine kurze Beschreibung des Bildes, welche als alternativer Text dient. Blog Bitte einen Artikel in Markdown eingeben. Blogeinträge Blogeintrag Eine durch Menschen lesbarer Bezeichner dieses Bildes. Bild Bilder Veröffentlicht Slug Titel 