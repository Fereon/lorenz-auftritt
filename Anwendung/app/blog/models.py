"""Submodule containing the orm models."""
import re
from io import BytesIO
from uuid import uuid4
from PIL import Image as PImage
from django.db import models
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.core.files.images import ImageFile

class Entry(models.Model):
    """
    This model is designated to hold data from for a blog entry.
    As blog entries may contain markdown, instances of this model should
    be created and edited by authenticated and trusted users only.
    """
    title = models.CharField(
        max_length=254,
        verbose_name=_('title'),
    )
    image = models.ForeignKey(
        'Image',
        on_delete=models.SET_NULL,
        null=True,
        verbose_name=_('image'),
    )
    article = models.TextField(
        help_text=_('Please enter an article body in markdown.')
    )
    published = models.DateTimeField(
        default=now,
        verbose_name=_('published'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('slug'),
    )

    class Meta:
        verbose_name = _('blog entry')
        verbose_name_plural = _('blog entries')
        ordering=['-published']

    def get_absolute_url(self):
        return reverse('blog:entry', kwargs={'slug': self.slug})

    def __str__(self):
        return str(self.title)

def resize(image, max_width):
    if image.width > max_width:
        height = round(
            ( max_width/image.width ) * image.height
        )
        return image.resize((max_width, height))
    else:
        return image

class Image(models.Model):
    """
    This model is designated to hold and manage image data for blog entries.
    """
    name = models.CharField(
        max_length=127,
        help_text=_('human readable name of this image.'),
        unique=True,
    )
    source = models.ImageField()
    large = models.ImageField(
        editable=False,
        null=True,
    )
    medium = models.ImageField(
        editable=False,
        null=True,
    )
    small = models.ImageField(
        editable=False,
        null=True,
    )
    description = models.CharField(
        max_length=511,
        help_text=_('A short description of the image used as alternate text.'),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.pk is not None:
            self._cache_source = self.source.file

    class Meta:
        verbose_name = _('image')
        verbose_name_plural = _('images')
    
    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        if self.pk is None or self.source.file is not self._cache_source:
            img = PImage.open(self.source.file)

            ext_list = re.findall(r'.[A-za-z0-9]+$', self.source.name)
            if len(ext_list) > 0:
                ext = ext_list[-1]
            else:
                ext = None

            new_name = str(uuid4())

            large_bytes = BytesIO()
            large_image = resize(img, 1280)
            large_image.save(large_bytes, format='WebP', method=6)
            self.large = ImageFile(large_bytes, 'blog/' + new_name + '-large.webp')

            medium_bytes = BytesIO()
            medium_image = resize(img, 780)
            medium_image.save(medium_bytes, format='WebP', method=6)
            self.medium = ImageFile(medium_bytes, 'blog/' + new_name + '-medium.webp')

            small_bytes = BytesIO()
            small_image = resize(img, 380)
            small_image.save(small_bytes, format='WebP', method=6)
            self.small = ImageFile(small_bytes, 'blog/' + new_name + '-small.webp')

            if ext:
                new_name += ext
            else:
                new_name += f".{img.format}"

            self.source = ImageFile(self.source.file, 'blog/' + new_name)

            large_image.close()
            medium_image.close()
            small_image.close()

        super().save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
        )

    def __str__(self):
        return str(self.name)
