"""Submodule containing the sitemap configuration."""
from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from .models import Entry

class StaticBlogListMap(Sitemap):
    """
    This Class is used for generating a sitemap entry for the blog page. 
    """
    changefreq = 'weekly'
    priority = 0.7
    protocol = 'https'

    def items(self):
        """
        Returns the namespaces of the associated page.
        """
        return ['blog:list']

    def location(self, item):
        """
        Returns the path of the associated page.
        """
        return reverse(item)

class BlogSitemap(Sitemap):
    """
    This Class is used for generating sitemap entries for individual 
    blog entries.
    """
    changefreq = "never"
    priority = 0.6
    protocol = 'https'

    def items(self):
        """
        Returns the namespace of the associated pages.
        """
        return Entry.objects.all()

    def lastmod(self, obj):
        """
        Returns the publish dates as date of last change
        of the associated pages.
        """
        return obj.published
