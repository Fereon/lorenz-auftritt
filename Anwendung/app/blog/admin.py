"""Registration of models for the admin pages."""
from django.contrib import admin
from .models import Entry, Image

admin.site.register(Entry)
admin.site.register(Image)
