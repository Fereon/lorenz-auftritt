"""Submodule containing the definition of tags."""
import markdown as mk
from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter()
def markdown(value):
    """Converts markdown into html."""
    return mark_safe(
        mk.markdown(
            conditional_escape(value)
        )
    )
