"""Submodule containing the paths for this app."""
from django.urls import path
from django.views.generic import ListView, DetailView
from .models import Entry

app_name = 'blog'
urlpatterns = [
    path(
        '',
        ListView.as_view(
            model=Entry,
            paginate_by=8,
        ),
        name='list'
    ),
    path(
        '<slug:slug>/',
        DetailView.as_view(
            model=Entry,
        ),
        name='entry'
    ),
]
