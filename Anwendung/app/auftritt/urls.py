"""auftritt URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import path, include
from django.utils.translation import gettext_lazy as _

# for generating a concise sitemap
# For more, please see: https://docs.djangoproject.com/en/4.1/ref/contrib/sitemaps/
from basic.sitemaps import StaticBasicMap
from blog.sitemaps import StaticBlogListMap, BlogSitemap
from contact.sitemaps import StaticContactMap

sitemaps = {
    'basic': StaticBasicMap,
    'blog_list' : StaticBlogListMap,
    'blog_entries': BlogSitemap,
    'contact' : StaticContactMap,
}

# Admin pages and sitemap paths
urlpatterns = [
    path(settings.ADMIN_URL + '/', admin.site.urls),
    path(
        'sitemap.xml',
        sitemap,
        {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap'
    ),
]

# Paths for all other
urlpatterns += i18n_patterns(
    path('', include('basic.urls'), name='basic'),
    path(_('kontakt/'), include('contact.urls'), name='contact'),
    path(_('blog/'), include('blog.urls'), name='blog'),
    prefix_default_language=False
)

# Paths for uploaded resources during development and debug
if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )
