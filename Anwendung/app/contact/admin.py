"""Registration of models for the admin pages."""
from django.contrib import admin
from .models import Contact

admin.site.register(Contact)
