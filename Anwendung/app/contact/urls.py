"""Submodule containing the paths for this app."""
from django.urls import path, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, TemplateView
from .forms import ContactForm

app_name = 'contact'
urlpatterns = [
    path(
        '',
        CreateView.as_view(
            template_name='contact/contact_form.html',
            form_class=ContactForm,
            success_url=reverse_lazy('contact:sent'),
        ),
        name='form'
    ),
    path(
        _('sent/'),
        TemplateView.as_view(template_name='contact/contact_sent.html'),
        name='sent'
    ),
]
