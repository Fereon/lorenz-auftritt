"""Submodule containing the sitemap configuration."""
from django.contrib.sitemaps import Sitemap
from django.urls import reverse

class StaticContactMap(Sitemap):
    """
    This Class is used for generating sitemap entries for the contact pages.
    """
    changefreq = 'yearly'
    protocol = 'https'

    def items(self):
        """
        Returns the namespaces of the associated pages.
        """
        return [
            'contact:form',
            'contact:sent',
        ]

    def location(self, item):
        """
        Returns the paths of the associated pages.
        """
        return reverse(item)

    def priority(self, item):
        """
        Returns the priority of the associated pages.
        """
        return {
            'contact:form': 0.7,
            'contact:sent': 0.0,
        }[item]
