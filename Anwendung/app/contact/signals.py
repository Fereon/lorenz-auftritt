"""Submodule containing signals for automation."""
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.conf import settings
from .models import Contact

@receiver(post_save, sender=Contact)
def email_notification(sender, **kwargs):
    """
    This functions is for sending an email notification
    whenever a new contact request has been processed.
    """
    if kwargs.get('created'):
        send_mail(
            subject='New Contact Request',
            message='A new contact request has just been processed.',
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[settings.EMAIL_HOST_USER],
        )
