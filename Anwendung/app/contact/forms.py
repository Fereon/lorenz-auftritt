"""Submodule containing form classes for this app."""
from django.forms import ModelForm, BooleanField
from django.utils.translation import gettext_lazy as _
from .models import Contact

class ContactForm(ModelForm):
    """
    This model form is designated to process contact requests.
    """
    class Meta:
        model = Contact
        fields = ['name', 'email', 'message']
    consent = BooleanField(
        required=True,
        label=_('I give my consent for processing of my personal \
data according to this sites privacy policy'),
        initial=False,
    )
    