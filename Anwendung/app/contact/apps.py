"""Submodule containing the application configuration."""
from django.apps import AppConfig
from django.core.signals import request_finished

class ContactConfig(AppConfig):
    """Django app config"""
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'contact'

    def ready(self):
        # additional signal for email notifications
        from . import signals
        request_finished.connect(signals.email_notification)
