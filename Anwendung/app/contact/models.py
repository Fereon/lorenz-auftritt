"""Submodule containing the orm models."""
from datetime import timedelta
from django.db import models
from django.utils.html import escape
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

class ContactManager(models.Manager):
    """
    This model manager is designated to manage the contact model ```Contact```.
    """
    def privacy_purge(self):
        """
        Function for purging expired contact models inline with the GDPR
        declaration of the website.
        """
        old = now() - timedelta(days=14)
        return super().get_queryset().filter(
            created__lte=old
        ).delete()

class Contact(models.Model):
    """
    This model is designated to hold user data sent in using a contact form.
    """
    name = models.CharField(
        max_length=254,
        verbose_name=_('name'),
    )
    email = models.EmailField(
        verbose_name=_('email'),
    )
    message = models.TextField(
        verbose_name=_('message')
    )
    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('created'),
    )
    objects = ContactManager()

    class Meta:
        verbose_name = _('contact')
        verbose_name_plural = _('contacts')

    def clean(self):
        """
        Cleans the model data by escaping HTML characters.
        As this model will only hold user input data, none may be trusted.
        This will break email addresses with special characters.
        """
        self.name = escape(self.name)
        self.message = escape(self.message)
        self.email = escape(self.email)
        super().clean()

    def __str__(self):
        return str(self.name)
