"""Submodule containing the paths for this app."""
from django.urls import path
from django.views.generic import TemplateView
from django.utils.translation import gettext_lazy as _

app_name = 'basic'
urlpatterns = [
    path('', TemplateView.as_view(template_name='basic/home.html'), name='home'),
    path(_('about/'), TemplateView.as_view(template_name='basic/about.html'), name='about'),
    path(_('imprint/'), TemplateView.as_view(template_name='basic/imprint.html'), name='imprint'),
    path(_('privacy/'), TemplateView.as_view(template_name='basic/privacy.html'), name='privacy'),
]