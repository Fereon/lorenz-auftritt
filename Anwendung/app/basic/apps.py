"""Submodule containing the application configuration."""
from django.apps import AppConfig

class BasicConfig(AppConfig):
    """Django app config"""
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'basic'
