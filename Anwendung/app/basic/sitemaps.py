"""Submodule containing the sitemap configuration."""
from django.contrib.sitemaps import Sitemap
from django.urls import reverse

class StaticBasicMap(Sitemap):
    """
    This Class is used for generating sitemap entries for the home,
    about, imprint and privacy pages.
    """
    changefreq = 'yearly'
    protocol = 'https'

    def items(self):
        """
        Returns the namespaces of the associated pages.
        """
        return [
            'basic:home',
            'basic:about',
            'basic:imprint',
            'basic:privacy',
        ]

    def location(self, item):
        """
        Returns the paths of the associated pages.
        """
        return reverse(item)

    def priority(self, item):
        """
        Returns the priority of the associated pages.
        """
        return {
            'basic:home': 1.0,
            'basic:about': 0.7,
            'basic:imprint': 0.3,
            'basic:privacy': 0.3,
        }[item]