// Adding accessibility features to menu

// Grabbing required elements
const checkbox = document.getElementById("menu-btn");
const header_nav = document.getElementById("header-nav");

// Set inert for mobile and tablet
if ( window.innerWidth < 1280 && !checkbox.checked ) {
    header_nav.setAttribute("inert", "true");
}

// Set inert for menu toggle
function change_checkbox() {
    if ( checkbox.checked ) {
        header_nav.removeAttribute("inert");
    } else {
        header_nav.setAttribute("inert", "true");
    }
}
checkbox.addEventListener("change", change_checkbox);

// Set menu toggle
document.getElementById("menu-icon").addEventListener("keydown", (event) => {
    if ( event.key === "Enter" || event.key === " " ){
        checkbox.checked = !checkbox.checked;
        change_checkbox()
    }
});

// Look for break points and set inter appropriately
window.addEventListener('resize', () => {
    if ( window.innerWidth < 1280 && !checkbox.checked ) {
        header_nav.setAttribute("inert", "true");
    } else if ( window.innerWidth >= 1280 ) {
        header_nav.removeAttribute("inert");
    }
}); 