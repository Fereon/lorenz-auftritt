#!/bin/bash

sudo docker-compose -f /home/lorenz/lorenz-auftritt/Anwendung/docker-compose.prod.yml exec nginx /bin/ash -c "rm /var/log/nginx/error.log; rm /var/log/nginx/access.log; nginx -s reload"
