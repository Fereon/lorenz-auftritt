#!/bin/bash

sudo docker-compose -f /home/lorenz/lorenz-auftritt/Anwendung/docker-compose.prod.yml exec certbot certbot renew --force-renewal
sudo docker-compose -f /home/lorenz/lorenz-auftritt/Anwendung/docker-compose.prod.yml exec nginx /bin/ash -c "nginx -s reload"
