# Verteilungsdiagramme

Die Anwendung wird in verschiedene Bestandteile unterteilt. Für die Entwicklung und den Betrieb werden jeweils zwei unterschiedliche Verteilungen verwendet. 

## Entwicklung
![Verteilungsdiagramm in der Entwicklung](./verteilungsdiagramm-entwicklung.png)

Die Applikation wird in einem *Docker*-Container betrieben und entwickelt. Hierbei wird der durch das Rahmenwerk zur Verfügung gestellte Testserver eingesetzt.

## Betrieb
![Verteilungsdiagramm im Betrieb](./verteilungsdiagramm-betrieb.png)

Im Betrieb wird eine Unterteilung in weitere Bestandteile (*Services*) vorgenommen. Die für die TLS-Verschlüsselung notwendigen Zertifikate werden durch *Certbot* verwaltet und von Webserver und Reverse-Proxy *Nginx* verwendet. Letzterer ist für das Bereitstellen von statischen und hochgeladenen Inhalten, sowie Caching verantwortlich. Die Applikation wird mithilfe des Webservers *Gunicorn* betrieben.

Für die Datenbank (*database*), die statischen Inhalte (*static*) und durch den Administrator hochgeladenen Inhalte (*media*) werden entsprechende *Volumes* verwendet.
