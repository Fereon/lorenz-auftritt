# Aufteilung

Die Django-Applikation nach den üblichen Konventionen in verschiedene Module unterteil.

## Moduldiagramm

Die Applikation wird in die Module ```auftritt```, ```contact```, ```blog``` und ```basic``` aufgeteilt.

![Moduldiagramm](./auftritt.png)

### auftritt
Das Modul ```auftritt``` enthält alle Einstellungen für die Django-Applikation, sowie die Endpunkte für das WSGI und ASGI.

![Diagramm "auftritt"](./auftritt.png)

### basic
Das Modul ```basic``` enthält die nötigen Ressourcen für die statischen Seiten, sowie jene die mit ```blog``` und ```contact``` geteilt werden (Zbs. Favicons und CSS-Dateien).

![Diagramm "basic"](./basic.png)

### blog
Das Modul ```basic``` enthält die nötigen Ressourcen für den Blog und deren Einträge.

![Diagramm "blog"](./blog.png)

### contact
Das Modul ```basic``` enthält die nötigen Ressourcen für die Kontaktunterseite.

![Diagramm "contact"](./contact.png)