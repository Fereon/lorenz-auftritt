# Objektrelationales Modell
Zur Abbildung der Datenbank wird das ORM-System von Django verwendet. Hierbei werden Klassen definiert, welche auf Tabellen in der Datenbank abgebildet werden. Änderungen am Modell werden automatisch auf die Datenbank übertragen.

Die Module ```blog``` und ```contact``` definieren Modelle jeweils in den Dateien ```models.py```. Geschachtelte Klassen zeigen auf die umgebenden mit einem leeren Kreis.

## Blog
Für den Blog wird eine Klasse für die Blogeinträge ```Entry``` und eine für die Titelbilder ```Image``` definiert.

![Blog ORM](./blog-orm.svg)

# Kontakt
Zum Erfassen und Verwalten der Kontaktanfragen wird die Klasse ```Contact``` definiert.

![Contakt ORM](./contact-orm.svg)