# Vorläufiges Pflichtenheft

Nach einer kurzen Urlaubspause habe ich die Anforderungsanalyse fortgeführt. Hierfür wurden basierend auf dem Lastenheft eine [Sitemap](../../Anforderungsanalyse/Artefakte-Pflichtenheft/Sitemap/sitemap.svg), ein [Use-Case-Diagramm](../../Anforderungsanalyse/Artefakte-Pflichtenheft/Usecases/Usecases.md), sowie ein [Klassendiagramm](../../Anforderungsanalyse/Artefakte-Pflichtenheft/UML-Klassen/klassendiagramm.svg) für die dynamischen Inhalte der Webseite erstellt.

Mithilfe dieser Artefakte wurde das [Pflichtenheft](../../Anforderungsanalyse/Pflichtenheft.md) geschrieben. Ziel dieses Dokuments ist eine Übersicht zu bieten. Hierbei werden die Anforderungen an die erste Version, sozusagen das *"minimal viable product"*, festgehalten. Nach in Beitriebnahme der Webseite, wird diese mit einem agilen Ansatz weiterentwickelt.

Im nächsten Schritt erfolgt der Entwurf des Internetauftritts.

![UML Klassendiagramm](../../Anforderungsanalyse/Artefakte-Pflichtenheft/Sitemap/sitemap.svg)