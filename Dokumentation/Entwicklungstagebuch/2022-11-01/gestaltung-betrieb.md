# Gestaltung und Inbetriebnahme der Webseite

![Blume](blume.jpg)

Über die letzten 3 Wochen habe ich die Webseite ausgestaltet und vorzeitig in Betrieb genommen. Hierbei habe ich mit dem [„Mobile first“-Grundsatz](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Responsive/Mobile_first) erst die Mobile- und danach die Desktop- und Tabletansicht umgesetzt. Im folgenden lege ich mein Vorgehen grob dar.

Wie beim Entwurf wird auch bei der Umsetzung der Gestaltung einfache Gestaltungsmuster angewendet. Zum einen wird die [Drittel-Regel](https://de.wikipedia.org/wiki/Drittel-Regel), zum anderen der goldene Schnitt eingesetzt, um die Seite ausgewogen wirken zu lassen. Aus praktischen Gründen der Lesbarkeit wird eine Ausnahme eingeführt: Textblöcke werden auf eine maximale Zeilenlänge von maximal 64 em beschränkt.

## Typographie

Zur Darstellung der Texte habe mich für die serifenlose Schriftart [Raleway](https://fonts.google.com/specimen/Raleway) entschieden. Auf mich wirkt diese Schriftart sowohl klar, als auch minimal und seriös. Ich habe mich bewusst gegen Einsatz von zwei verschiedenen Schriftarten für Titel und Textblöcke entschieden. (Ich sehe schlicht den Bedarf nicht gegeben.) Die Schriftart wird, sofern sie nicht auf dem Gerät des Nutzenden installiert ist, vom Server angeboten.

## Barrierefreiheit

Ich möchte, dass das Internet allen offen steht. Aus diesem Grund ist mir die Barrierefreiheit meiner Seite wichtig. Hierbei habe ich die Entwicklerwerkzeuge von Mozilla Firefox und die Leitfäden von [MDN](https://developer.mozilla.org/en-US/docs/Web/Accessibility) und [Google Chrome Developers](https://youtube.com/playlist?list=PLNYkxOF6rcICWx0C9LVWWVqvHlYJyqw7g) angewendet.

## Menü und Navigation

Die Navigation der Seite wurde für Mobiltelefone und Tabletgeräte mit einem Hamburgermenü umgesetzt. Im Falle der Desktopvariante ist die Navigation in der Headerleiste eingebettet.

Das Hamburgermenü wurde mit HTML und CSS umgesetzt. Hierbei wird eine versteckte Checkbox mit Label eingesetzt. Dies ermöglicht auch bei deaktivierter Scriptausführung (zbs. durch NoScript) eine nutzbare Seite.

Zur Gewährleistung der Barrierefreiheit wurde Javascript eingesetzt. Das Menü kann somit auch mit der Tastatur oder einem Screenreader bedient werden.

## Hauptseite

Für die Hauptseite habe ich mich für ein Motiv von mehreren sich überlagernden Wellen entschieden. Hierbei „schwappen“ jeweils im Wechsel verschiedenfarbige Wellen übereinander, wodurch eine visuell ansprechende Dynamik geboten wird. (Die Wellen wurden mit einem Generator erstellt: [Hakei](https://haikei.app/)) Zwischen diesen optischen Trennern sind Abschnitte eingebettet, welche relevante Unterseiten mit einem „Call to Action“ bewerben. Ich habe mich an dieser Stelle gegen weitere Animationen oder Parallaxen-Effekte entschieden, um eine visuelle Überreizung zu vermeiden.

Einzelne Elemente und Ideen der Hauptseite werden in den Unterseiten aufgenommen. So wird der Wellenübergang von der Headerleiste in den Hauptkörper der Seite übernommen. 

## Blog

Die Übersicht über die Blogeinträge besteht aus mehreren Artikelelementen, welche das Titelbild, den Titel und das Veröffentlichungsdatum festhalten. Das Titelbild wird hierbei unter Erhalt der Seitenverhältnisse gestreckt. Dies bietet zwar eine gute Portabilität, jedoch muss Acht bei der Wahl der Bilder gegeben werden: Mitunter werden Ränder abgeschnitten.

## Vorgezogene Inbetriebnahme

In zwei Wochen, am 15. November 2022, findet der [35. Industrie-Tag Informations-Technologie](https://www.uni-halle.de/uzi/veranstaltungen/35it/) an meiner Universität (Martin-Luther-Universität Halle-Wittenberg) statt. Zu diesem Zeitpunkt möchte ich, dass die Webseite nicht nur online ist, sondern auch durch gängige Suchmaschinen indiziert wurde. Aus diesem Grund habe ich die Inbetriebnahme auf den 29. November 2022 vorgezogen. Dies heißt nicht, dass auf die Erfüllung von Anforderungen aus dem Pflichtenheft verzichtet wurde, sondern, dass die Dokumentation nicht zu meiner Zufriedenstellung vervollständigt ist.

Folgendes bleibt entsprechend zu erledigen:
- Die Sitemapfunktionalität muss im Feinentwurf ergänzt werden.
- Allen Modulen, Klassen und Funktionen sind Doctstirngs hinzuzufügen, und eine Dokumentation mit Pydoc ist zu generieren. 
- Eine Installationsanleitung ist zu schreiben.
- Die Abnahmetest sind formell durchzuführen, auszuwerten und zu dokumentieren.