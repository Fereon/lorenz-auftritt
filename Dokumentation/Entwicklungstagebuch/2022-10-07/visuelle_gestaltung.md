# Visueller Entwurf

![Farbpalette](../../Entwurf/Feinentwurf/Visuell/Farbpalette/AdobeColor-Auftritt.jpeg)

Zur Planung der visuellen Gestaltung wurden eine Farbpalette und ein grobes Layout entworfen.

Die Farbpalette wurde mithilfe von [Adobe Color](https://color.adobe.com/de/create/color-wheel) gestaltet. Hierbei wurden zwei komplementäre Farbtöne, sowie verschiedene Schattierungen dieser ausgesucht. Als Grundton fungiert ein sanftes und schwach gesättigtes Blau, welches mich an weiche und gut eingetragene Baumwollkleidung erinnert. Komplementär hierzu steht ein Gelb- bis Braunton, der mich an Sand oder Lehmerde erinnert. 
Bei näher Betrachtung gibt mir diese Palette das Gefühl an einem Sandstrand bei Wind  und rauer See zu stehen. Zwischen meinen Fingern spüre ich den Stoff einer Windjacke oder einen Halm trockenem Strandgras.

Das Layout der Seite folgt aus der Palette: Klare Geraden sollen mit sanften Kurven, Rundungen und Kreisen aufgebrochen werden. Das Layout soll möglichst natürlich wirken.

Für den Entwurf des [Layouts](../../Entwurf/Feinentwurf/Visuell/Layout/Auftritt.pdf) habe ich das Werkzeug [Figma](https://www.figma.com/) genutzt. Für die Wellen am unteren Rand der Navigationsleiste habe ich [Shape Dividers](https://www.shapedivider.app/) eingesetzt.

Im nächsten Schritt werde ich das Markup und die Stylesheets der Seiten gestalten. Ich werde hierbei die Entwicklerwerkzeuge meines Webbrowsers einsetzen, um das Verhalten der Seite bei verschiedenen Bildschirmgrößen zu prüfen. Den Auftritt möchte ich für Smartphones, Tablet- und Desktop-PCs optimieren. (Stichwort: Responsive Design)