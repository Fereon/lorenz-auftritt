# Vorläufiges Lastenheft

Mithilfe von ein paar handschriftlichen Notizen und Mindmaps konnten die groben Anforderungen ermittelt werden. Aus diesen wurde ein [Lastenheft](../../Anforderungsanalyse/Lastenheft.md) erstellt. Hierbei wurde zunächst festgehalten *WAS* die zu entwickelnde Software erreichen soll.

In diesem Zusammenhang wurde auch die Entscheidung getroffen, dass alle relevanten Dokumente in Markdown verfasst werden. Bisher habe ich zur Erzeugung solcher Dokumente das Textsatzsystem LaTeX verwendet und möchte nun Markdown mit der verwendeten Softwareentwicklungsplattform ([Codeberg](https://codeberg.org/)) "ausprobieren".

Im nächsten Schritt wird das Lastenheft verfeinert zum Pflichtenheft. In diesem wird zusätzlich ein Augenmerk gelegt auf *WIE* die Visionen des Projekts erreicht werden. 

Zu Übungszwecken versuche ich genau zwischen *WAS* und *WIE* zu trennen. So ist es durchaus angebracht die bisherige Lösung zum Hosting der Webseite, ein angemieteter *VPS* (*Virtual Private Server*), als Rahmenbedingung im Lastenheft aufzunehmen, doch ich entscheide mich, dass dies unter *WIE* der Internetauftritt umgesetzt werden kann fällt und somit in das Pflichtenheft aufgenommen wird.

![Schräges Foto vom Bildschirm mit Lastenheft](dutch-bildschirm.jpg)