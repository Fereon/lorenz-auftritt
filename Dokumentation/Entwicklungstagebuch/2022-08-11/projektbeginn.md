# Projektbeginn

Ziel dieses Projektes ist die Neugestaltung meines Internetauftritts. ([Webseite](https://www.lorenz-zahn.de/)) In diesem Zusammenhang möchte ich einige *"Best Practices"* und *Werkzeuge der Softwaretechnik* anwenden und vertiefen.

Zur Vorbereitung habe ich zunächst zwei Bücher vollständig gelesen:
1. Balzert, Helmut; *Lehrbruch der Softwaretechnik: Basiskonzepte und Requirements Engineering*, Spektrum Akademischer Verlag Heidelberg, 2009
2. Beaird, Jason; George, James; Walker, Alex; *The Principles of Beautiful Web Design*, SitePoint Pty. Ltd, 2020

Im ersten Schritt wurde eine Ablage (Git-Remote-Repository) unter [Codeberg](https://codeberg.org/Fereon/lorenz-auftritt) zur Versionsverwaltung eingerichtet. Alle Artefakte, welche im Laufe der Entwicklung entstehen, werden dort öffentlich gesammelt.

Im nächsten Schritt erfolgt die Anforderungsanalyse (Requirements Engineering). Hierzu werden zunächst einfache Mittel, wie *Notizen* und eine *Mindmap* verwendet, um ein Lastenheft zu erstellen.

![Ausarbeitung der Anforderungen](Schreibtisch.jpg)