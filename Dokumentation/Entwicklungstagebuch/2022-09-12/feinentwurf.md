# Feinentwurf und Implementierung

![Bildschirmfoto von einem Terminal](./Bildschirmfoto2022-09-12.png)

Im nächsten Schritt möchte ich den Feinentwurf und die Implementierung iterativ angehen. Zuerst werden statischen Seiten, danach das Kontaktformular und zuletzt der Blog schrittweise entworfen und implementiert. Hierbei werde ich mich zunächst auf das Datenmodell und die Geschäftslogik konzentrieren. Die visuelle und inhaltliche Gestaltung folgt später.

Zu diesem Zweck habe ich die [Verteilungsdiagramme](../../Entwurf/Verteilungsdiagramm/verteilunsdiagramm.md) zu einem großen Teil umgesetzt. Genauer wurde das Verteilungsdiagramm für die Entwicklung vollständig und und jenes für den Betrieb mit ein paar vorläufigen Konfigurationen erstellt.

_Hinweis:_ Zur Umsetzung wurde eine Anleitung als Hilfestellung verwendet. Hier ein Link zum Blogeintrag: [Dockerizing Django with Postgres, Gunicorn, and Nginx](https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/)