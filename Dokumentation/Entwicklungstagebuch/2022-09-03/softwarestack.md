# Zusammenstellen des Softwarestacks

![Verteilungsdiagramm](../../Entwurf/Verteilungsdiagramm/verteilungsdiagramm-betrieb.png)

Zur Realisierung des Internetauftritts greife ich auf bekannte Komponenten und Muster zurück.

Zuerst definiere und unterteile ich den Softwarestack in verschiedene Dienste, die mithilfe von *Docker* und *Docker Compose* zu einem Ganzen orchestriert werden. Hierbei kommt bereits bewährte und vertrauenswürdige Standardsoftware zum Einsatz. Ein Stück weit möchte zeigen, dass ich in der Lage bin diese effektiv einzusetzen.

Zur Dokumentation werden [Verteilungsdiagramme](../../Entwurf/Verteilungsdiagramm/verteilunsdiagramm.md) eingesetzt.
## Webframework: Django

Der Internetauftritt wird mithilfe von [Django](https://www.djangoproject.com/) umgesetzt. Es handelt sich hierbei um keine zufällige Wahl. Zum Einen sind mit die Programmiersprache Python und dieses Webframework bereits bekannt, zum Anderen werden sich einige der Features als durchaus nützlich erweisen. So bietet, zum Beispiel, das *admin*-Packet ein leistungsfähiges CMS.

Mit Model-View-Template wird die Architektur der Applikation durch das Rahmenwerk vorgegeben.

## Datenbank

Zur Verwaltung der Kontaktanfragen und der Blogeinträge wird eine relationale Datenbank in Verbindung des ORM von Django verwendet. Aufgrund knapper Systemresourcen und geringen Komplexität der Datenbank wird SQLite hier genügen. Eine spätere Migration nach PostgreSQL wird im Hinterkopf behalten. 

## WSGI-Server

Zur Betreibung der Django-Applikation wird ein WSGI- (Web Server Gateway Interface) oder ASGI-kompatibler (Asynchronous Server Gateway Interface) Webserver benötigt. 

Zwecks einer möglichst einfacheren Einrichtung und Schonung von Resourcen, werde ich vorerst auf [Gunicorn](https://gunicorn.org/) zurückgreifen. Später werde ich vielleicht auch einen ASGI-Server, wie [Daphne](https://github.com/django/daphne) oder [Hypercorn](https://github.com/pgjones/hypercorn) ausprobieren.

## Webserver und Reverse-Proxy
Zum Bereitstellen von statischen Inhalten und zur verschlüsseln der Verbindungen werde ich [Nginx](https://www.nginx.com/) als Web- bzws. Reverse-Proxy-Server einsetzen. Ich erhoffe mir dadurch in Zukunft eine erleichterte Einbindung von weiteren Diensten in die bestehende Infrastruktur.

## Verwaltung von Zertifikaten
Zur automatischen Verwaltung der für die Verschlüsselung benötigten Zertifikaten wird [Certbot](https://certbot.eff.org/) eingesetzt. Hierbei wird auf die Zertifizierungsstelle [Let’s Encrypt](https://letsencrypt.org/) gesetzt.