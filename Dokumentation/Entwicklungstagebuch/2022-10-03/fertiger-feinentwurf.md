# Feinentwurf: Fertig

![Blog ORM](../../Entwurf/Feinentwurf/ORM/blog-orm.svg)

Nach einer längeren Pause wurde der Feinentwurf vorläufig fertiggestellt. Parallel hierzu erfolgte zusätzlich die Implementierung. Letztere ist noch nicht abgeschlossen, da noch kein Entwurf für die visuelle Gestaltung vorliegt.

Für die visuelle Gestaltung muss ein passender Aufbau, eine Farbpalette und Typographie gefunden und umgesetzt werden. Ich bin kein Künstler, aber dennoch zuversichtlich, dass ich dieser Aufgabe gewachsen bin!