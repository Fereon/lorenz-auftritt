# Pflichtenheft: Internetauftritt

| Version | Autor       | Quelle | Status    | Datum      | Kommentar |
| :---    | :---        | :---   | :---      | :---       | :---      |
| 0.1     | Lorenz Zahn | -      | Vorläufig | 2022-08-29 | -         |

Voreinstellungen *(Kursiv dargestellt)*:  
Priorität aus Auftraggebersicht = {hoch, *mittel*, niedrig}  
Priorität aus Auftragnehmersicht = {hoch, *mittel*, niedrig}  
Stabilität der Anforderung = {fest, *gefestigt*, volatil}  
Kritikalität der Anforderung = {hoch, mittel, *niedrig*, keine}  
Entwicklungsrisiko der Anforderung = {hoch, mittel, *niedrig*}  

## Visionen und Ziele
**/V10/** (/LV10/) Besucher erhalten innerhalb kurzer Zeit einen Eindruck über mich (Lorenz Zahn), meine Fähigkeiten, Kompetenzen und Qualifikationen.  
**/V20/** (/LV20/) Besucher werden ermutigt mit mir in Kontakt zu treten.  
**/Z10/** (/LZ10/) Der Internetauftritt bietet Besuchern eine Sammlung von Informationen zu meiner Person an.  
**/Z20/** (/LZ20/) Der Internetauftritt bietet Besuchern die Möglichkeit die Entwicklung meines Portfolio mitzuverfolgen sowie nachzuvollziehen.  
**/Z30/** Einem Administrator soll in der Lage sein dem Internetauftritt um Inhalte in Form von Blogbeiträgen ohne Unterbrechung des Betriebs zu erweitern.

## Rahmenbedingungen
**/R10/** (/LR10/) Der Internetauftritt ist eine öffentlich zugängliche Webseite.  
**/R20/** (/LR20/) Zielgruppe sind alle Interessenten mit einem internetfähigen Gerät und einem herkömmlichen Webbrowser.  
**/R21/** Der Internetauftritt soll mit verschiedenen Endgeräten (Smartphone, Tablet, Notebook und Desktop PC) skalieren.  
**/R22/** Der Internetauftritt muss mit den Webbrowsern *Mozilla Firefox*, *Apple Safari* und *Google Chrome* in der jeweils aktuellsten Version kompatibel sein.  
**/R30/** Der Betrieb des Systems muss unbeaufsichtigt ablaufen.  
**/R40/** Das System muss im Dauerbetrieb laufen.  
**/R40/** Das System muss auf einem *VPS* (Virtual Private Server) mit mindestens 1 Rechenkern, 512 MB RAM und 10 GB Festplattenspeicher unter *Ubuntu Linux 20.04 LTS 64-Bit* (oder neuer) ausgeführt werden.   
**/R50/** Zur Entwicklung und im Betrieb soll *Docker* zur Containervirtualisierung der einzelnen Komponenten eingesetzt werden.

## Kontext und Überblick
**/K10/** (/LK10/) Der Internetauftritt ist praktisch dauerhaft verfügbar.

## Funktionale Anforderungen
**/FA10/** Ein Besucher muss die Hauptseite des Internetauftritts aufrufen können.  
**/FA11/** Ein Besucher muss von der Hauptseite zu anderen Unterseiten navigieren können.  
**/FA20/** Ein Besucher muss eine Seite mit dem Impressum aufrufen können.  
**/FA30/** Ein Besucher muss eine Seite mit der Datenschutzerklärung aufrufen können.  
**/FA40/** Ein Besucher soll eine Seite mit dem Blog aufrufen können.  
**/FA41/** Ein Besucher soll einen beliebigen Blogeintrag auswählen, aufrufen und lesen können.  
**/FA50/** Ein Besucher soll eine Seite mit der "Über mich"-Rubrik aufrufen können.  
**/FA60/** Ein Besucher soll eine Seite mit Verweisen zu weiteren persönlichen Projekten aufrufen können.  
**/FA70/** Ein Besucher soll eine Kontaktanfrage absenden können.  
**/FA71/** Eine Kontaktanfrage bestehe aus dem Name, der Email und einer Nachricht. Diese Angaben werden durch den Besucher angegeben.  
**/FA80/** Ein Administrator soll Blogeinträge verfassen, bearbeiten und löschen können.  
**/FA81/** Ein Blogeintrag bestehe hierbei aus einem Titel, einem Titelbild, ein Datum, sowie einem Text.  
**/FA90/** Ein Administrator soll eine Kontaktanfrage aufrufen und löschen können.

## Qualitätsanforderungen
| Systemqualität        | sehr gut | gut  | normal | nicht relevant |
| :---                  | :--:     | :--: | :--:   | :--:           |
| **Funktionalität**    |          |      |        |                |
| Angemessenheit        |          |  X   |        |                |
| Genauigkeit           |          |      |   X    |                |
| Interoperabilität     |    X     |      |        |                |
| Sicherheit            |          |  X   |        |                |
| Konformität           |          |  X   |        |                |
| **Zuverlässigkeit**   |          |      |        |                |
| Reife                 |          |      |   X    |                |
| Fehlertoleranz        |          |      |   X    |                |
| Wiederherstellbarkeit |          |  X   |        |                |
| Konformität           |          |      |   X    |                |
| **Benutzbarkeit**     |          |      |        |                |
| Verständlichkeit      |     X    |      |        |                |
| Erlernbarkeit         |          |  X   |        |                |
| Bedienbarkeit         |          |  X   |        |                |
| Attraktivität         |     X    |      |        |                |
| Konformität           |     X    |      |        |                |
| **Effizienz**         |          |      |        |                |
| Zeitverhalten         |          |      |   X    |                |
| Verbrauchsverhalten   |          |      |   X    |                |
| Konformität           |          |      |   X    |                |
| **Wartbarkeit**       |          |      |        |                |
| Analysierbarkeit      |          |  X   |        |                |
| Änderbarkeit          |     X    |      |        |                |
| Stabilität            |          |      |   X    |                |
| Testbarkeit           |          |  X   |        |                |
| Konformität           |          |      |   X    |                |
| **Portabilität**      |          |      |        |                |
| Anpassbarkeit         |     X    |      |        |                |
| Installierbarkeit     |     X    |      |        |                |
| Koexistenz            |          |      |   X    |                |
| Austauschbarkeit      |          |      |   X    |                |
| Konformität           |          |      |   X    |                |


**/QFI10/** (/LQF30/) Der Internetauftritt muss mit herkömmlichen Webbrowsern interoperabel sein.  
**/QFS10/** (/LQF20/) Das System und ein mögliches *CMS* muss vor unberechtigten Zugriffen geschützt sein.   
**/QFK10/** (/LQF10/) Der Internetauftritt muss allen gesetzlichen Anforderungen an eine Webseite (zbs. Datenschutz und Impressum) genügen.  
**/QFK20/** Der Internetauftritt muss für die Indexierung durch Suchmaschinen optimiert sein. (SEO)

**/QZV10/** Der Internetauftritt soll bei kleineren Fehlern den HTTP-Statuscode 503 ausgeben, jedoch weiterhin für Anfragen verfügbar bleiben.  
**/QZW10/** Der Internetauftritt muss möglichst einfach zu wiederherstellen sein. (Zbs. durch Neustart oder Rebuild der virtualisierten Container.)  

**/QBV10/** (/LQB10/) Der Internetauftritt muss für Besucher mit durchschnittlichen technischer Erfahrung sowohl intuitiv, als auch attraktiv sein.  
**/QBA10/** Der Internetauftritt muss eine attraktive visuelle Gestaltung für mögliche Interessenten bieten.  
**/QBB10/** (/LQB20/) Der Internetauftritt kann in einer späteren Version in mehreren Sprachen verfügbar sein.  
**/QBK10/** Der Internetauftritt soll möglichst konform mit den aktuellen *Web Content Accessibility Guidelines* sein.

**/QWÄ10/** (/LQW10/) Der Internetauftritt und das System müssen auf Erweiterbarkeit ausgelegt sein.  
**/QWT10/** Neu hinzugefügte oder geänderte Funktionalitäten müssen durch entsprechende Werkzeuge getestet werden können.  

**/QWPA10/** Das System muss auf die verfügbaren Hardwareressourcen angepasst werden können. (Zbs. Upgrade des VPS)  
**/QWPI10/** Das System muss möglichst rasch und reibungslos auf einen anderen VPS migrierbar sein.

## Abnahmekriterien
**/AK10/** Jede Seite des Internetauftritts muss mit den Webbrowsern *Mozilla Firefox*, *Apple Safari* und *Google Chrome* auf einem Desktop PC sowie Smartphone korrekt funktionieren und dargestellt werden.  
**/AK20/** Die Webseite muss mit dem Entwicklungswerkzeug *Google Lighthouse* jeweils einen Score von 100 in den Kategorien *Performance*, *Accessibility*, *Best Practices* und *SEO* erreichen.  
**/AK30/** Das Absenden, Empfangen, Lesen und wieder Löschen von Kontaktanfragen muss getestet werden.  
**/AK40/** Das Verfassen, Bearbeiten, Lesen und Löschen von Blogeinträgen muss getestet werden.  
**/AK41/** Das Hochladen, Zuweisen und Löschen von Titelbildern muss getestet werden.  
**/AK50/** Ein Belastungstest mit etwa 200 Anfragen pro Minute über 5 Minuten muss erfolgreich ohne Fehler absolviert werden.  

## Glossar
**Besucher**  Interessent, welcher den Internetauftritt mit einem Webbrowser aufruft oder *"besucht"*.  
**Blog** Eine Webseite, welche öffentliches Tagebuch fungiert.  
**Container** Virtuelle Umgebung, in der eine Software (oder Softwarekomponente) ausgeführt werden kann. Diese wird für gewöhnlich aus einem Abbild des lauffähigen (Sub-) Systems generiert.  
**Content Management System** Ein System zur Verwaltung von Inhalten auf einer Webseite.  
**Interessent** Natürliche Person, welche Interesse an dem Internetauftritt hat.  
**Rebuild** Neueinrichtung eines Containers.  
**Search Engine Optimization (SEO)** Optimierung einer Webseite mit Absicht die automatische Indexieren durch Suchmaschinen zu verbessern.   
**Virtual Private Server (VPS)** Eine meist angemietete virtuelle Maschine, welche die Rechenarchitektur eines real in Hardware oder hypothetischen Rechner abbildet.  
**Webbrowser** Ein Computerprogramm, welches zum Aufruf und Darstellen von Webseiten, Dokumenten und anderen Daten genutzt werden kann. Bekannte Vertreter sind *Mozilla Firefox*, *Google Chrome* und *Apple Safari*.