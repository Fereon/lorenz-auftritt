# Use Cases

![Use Case Diagramm](use-cases.svg)

**Use Case:** (/FA10/) Hauptseite aufrufen  
**Ziel:** Die Hauptseite der des Internetauftritts erhalten  
**Vorbedingung:** -  
**Nachbedingung Erfolg:** Besucher erhält die Hauptseite mit allen assoziierten Inhalten. (HTTP Statuscode 200)  
**Nachbedingung Fehlschlag:** Besucher erhält leere Seite mit HTTP Statuscode 503 oder eine andere Fehlernachricht  
**Akteure:** Besucher  
**Auslösendes Ereignis:** Anfrage über Webbrowser  
**Beschreibung:**  
**1** Der Besucher ruft die URL der Hauptseite mit einem Webbrowser auf  
**2** Der Besucher erhält die Hauptseite mit allen assoziierten Inhalten  
**3** Der Webbrowser des Besuchers stellt die Hauptseite korrekt dar  

**Use Case:** (/FA20/) Impressum aufrufen  
**Ziel:** Das Impressum der des Internetauftritts erhalten    
**Vorbedingung:** -  
**Nachbedingung Erfolg:** Analog zu "Hauptseite aufrufen"  
**Nachbedingung Fehlschlag:** Analog zu "Hauptseite aufrufen"  
**Akteure:** Besucher  
**Auslösendes Ereignis:** Anfrage über Webbrowser  
**Beschreibung:** Analog zu "Hauptseite aufrufen"  

**Use Case:** (/FA30/) Datenschutzerklärung aufrufen  
**Ziel:** Die Datenschutzerklärung der des Internetauftritts erhalten  
**Vorbedingung:** -  
**Nachbedingung Erfolg:** Analog zu "Hauptseite aufrufen"  
**Nachbedingung Fehlschlag:** Analog zu "Hauptseite aufrufen"  
**Akteure:** Besucher  
**Auslösendes Ereignis:** Anfrage über Webbrowser  
**Beschreibung:** Analog zu "Hauptseite aufrufen"

**Use Case:** (/FA40/) Blog aufrufen   
**Ziel:** Die Unterseite mit einer Auflistung aller Blogeinträge erhalten  
**Vorbedingung:** -  
**Nachbedingung Erfolg:** Analog zu "Hauptseite aufrufen"  
**Nachbedingung Fehlschlag:** Analog zu "Hauptseite aufrufen"  
**Akteure:** Besucher  
**Auslösendes Ereignis:** Anfrage über Webbrowser   
**Beschreibung:** Analog zu "Hauptseite aufrufen"  
**Erweiterung:** Blogeintrag aufrufen

**Use Case:** (/FA41/) Blogeintrag aufrufen  
**Ziel:** Die Unterseite mit dem gewählten Blogeintrag erhalten  
**Vorbedingung:** Der gesuchte Blogeintrag existiert  
**Nachbedingung Erfolg:** Analog zu "Hauptseite aufrufen"  
**Nachbedingung Fehlschlag:** Analog zu "Hauptseite aufrufen"  
**Akteure:** Besucher  
**Auslösendes Ereignis:** Anfrage über Webbrowser  
**Beschreibung:** Analog zu "Hauptseite aufrufen"

**Use Case:** (/FA50/) "Über mich" aufrufen  
**Ziel:** Die Unterseite mit der "Über mich"-Rubrik des Internetauftritts erhalten.  
**Vorbedingung:** -  
**Nachbedingung Erfolg:** Analog zu "Hauptseite aufrufen"  
**Nachbedingung Fehlschlag:** Analog zu "Hauptseite aufrufen"  
**Akteure:** Besucher  
**Auslösendes Ereignis:** Anfrage über Webbrowser  
**Beschreibung:** Analog zu "Hauptseite aufrufen"

**Use Case:** (/FA60/) Projekte aufrufen  
**Ziel:** Die Unterseite mit Verweisen zu weiteren persönlichen Projekten erhalten.  
**Vorbedingung:** -  
**Nachbedingung Erfolg:** Analog zu "Hauptseite aufrufen"  
**Nachbedingung Fehlschlag:** Analog zu "Hauptseite aufrufen"  
**Akteure:** Besucher  
**Auslösendes Ereignis:** Anfrage über Webbrowser  
**Beschreibung:** Analog zu "Hauptseite aufrufen"

**Use Case:** (/FA70/) Kontaktanfrage ausführen  
**Ziel:** Eine Kontaktanfrage an den Webmaster des Internetantritts senden.  
**Vorbedingung:** -  
**Nachbedingung Erfolg:** Eine Nachricht über Erfolg der Kontaktanfrage  
**Nachbedingung Fehlschlag:** Eine Nachricht über Fehlschlag der Kontaktanfrage  
**Akteure:** Besucher  
**Auslösendes Ereignis:** Anfrage über Webbrowser  
**Beschreibung:**  
**1** Der Besucher ruft die URL zur Kontaktanfrage mit einem Webbrowser auf.  
**2** Der Besucher erhält ein Kontaktformular, füllt dieses aus und schickt es ab.

**Use Case:** (/FA80/) Blogeintrag verwalten  
**Ziel:** Ein Blogeintrag wird verfasst, bearbeitet oder gelöscht.  
**Vorbedingung:** Der Akteur ist authentifiziert.  
**Nachbedingung Erfolg:** Die Änderungen werden übernommen.  
**Nachbedingung Fehlschlag:** Die Änderungen werden nicht übernommen und eine Fehlernachricht wird zurückgegeben.  
**Akteure:** Administrator  
**Auslösendes Ereignis:** Anfrage über Webbrowser

**Use Case:** (/FA90/) Kontaktanfrage verwalten  
**Ziel:** Eine Kontaktanfrage wird aufgerufen oder gelöscht.  
**Vorbedingung:** Eine Kontaktanfrage existiert und der Der Akteur ist authentifiziert.  
**Nachbedingung Erfolg:** Die Kontaktanfrage wird ausgegeben oder gelöscht.   
**Nachbedingung Fehlschlag:** Eine Fehlermeldung wird ausgegeben.  
**Akteure:** Administrator  
**Auslösendes Ereignis:** Anfrage über Webbrowser