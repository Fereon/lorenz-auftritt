# Lastenheft: Internetauftritt

| Version | Autor       | Quelle | Status    | Datum      | Kommentar |
| :---    | :---        | :---   | :---      | :---       | :---      |
| 0.1     | Lorenz Zahn | -      | Vorläufig | 2022-08-13 | -         |

Voreinstellungen *(Kursiv dargestellt)*:  
Priorität aus Auftraggebersicht = {hoch, *mittel*, niedrig}  
Priorität aus Auftragnehmersicht = {hoch, *mittel*, niedrig}  
Stabilität der Anforderung = {fest, *gefestigt*, volatil}  
Kritikalität der Anforderung = {hoch, mittel, *niedrig*, keine}  
Entwicklungsrisiko der Anforderung = {hoch, mittel, *niedrig*}  

## Visionen und Ziele
**/LV10/** Besucher erhalten innerhalb kurzer Zeit einen Eindruck über mich (Lorenz Zahn), meine Fähigkeiten, Kompetenzen und Qualifikationen.  
**/LV20/** Besucher werden ermutigt mit mir in Kontakt zu treten.  
**/LZ10/** Der Internetauftritt bietet Besuchern eine Sammlung von Informationen zu meiner Person an.  
**/LZ20/** Der Internetauftritt bietet Besuchern die Möglichkeit die Entwicklung meines Portfolio mitzuverfolgen sowie nachzuvollziehen.

## Rahmenbedingungen
**/LR10/** Der Internetauftritt ist eine öffentlich zugängliche Webseite.  
**/LR20/** Zielgruppe sind alle Interessenten mit einem internetfähigen Gerät und einem herkömmlichen Webbrowser.

## Kontext und Überblick
**/LK10/** Der Internetauftritt ist praktisch dauerhaft verfügbar.

## Funktionale Anforderungen
**/LFA10/** Der Internetauftritt muss eine Seite mit einem Impressum anbieten.  
**/LFA20/** Der Internetauftritt muss eine Seite mit einer Datenschutzerklärung anbieten.  
**/LFA30/** Der Internetauftritt soll eine Seite mit einem Kontaktformular anbieten.  
**/LFA40/** Der Internetauftritt soll eine Möglichkeit anbieten Verweise zu anderen persönlichen Projekten einzubetten.  
**/LFA50/** Der Internetauftritt soll einen Blog mit entsprechenden *CMS* (*Content Management System*) anbieten.

## Qualitätsanforderungen
| Systemqualität  | sehr gut | gut  | normal | nicht relevant |
| :---            | :--:     | :--: | :--:   | :--:           |
| Funktionalität  |          | X    |        |                |
| Zuverlässigkeit |          |      | X      |                |
| Benutzbarkeit   | X        |      |        |                |
| Effizienz       |          |      | X      |                |
| Wartbarkeit     |          | X    |        |                |
| Portabilität    |          | X    |        |                |

**/LQF10/** Der Internetauftritt muss allen gesetzlichen Anforderungen an eine Webseite (zbs. Datenschutz und Impressum) genügen.  
**/LQF20/** Das System und ein mögliches *CMS* muss vor unberechtigten Zugriffen geschützt sein.  
**/LQF30/** Der Internetauftritt muss mit herkömmlichen Webbrowsern interoperabel sein.

**/LQB10/** Der Internetauftritt muss für Besucher mit durchschnittlichen technischer Erfahrung sowohl intuitiv, als auch attraktiv sein.  
**/LQB20/** Der Internetauftritt soll in einer späteren Version in mehreren Sprachen verfügbar sein.

**/LQW10/** Der Internetauftritt und das System müssen auf Erweiterbarkeit ausgelegt sein.

## Glossar
**Besucher**  Interessent, welcher den Internetauftritt mit einem Webbrowser aufruft oder _"besucht"_.  
**Blog** Eine Webseite, welche öffentliches Tagebuch fungiert.  
**Content Management System** Ein System zur Verwaltung von Inhalten auf einer Webseite.  
**Interessent** Natürliche Person, welche Interesse an dem Internetauftritt hat.  
**Webbrowser** Ein Computerprogramm, welches zum Aufruf und Darstellen von Webseiten, Dokumenten und anderen Daten genutzt werden kann. Bekannte Vertreter sind _"Mozilla Firefox"_, _"Google Chrome"_ und _"Apple Safari"_.