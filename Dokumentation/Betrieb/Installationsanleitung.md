# Installationsanleitung

Diese Anleitung setzt grundlegende Kenntnisse im Umgang mit Linux Ubuntu Server (20.04 LTS oder neuer) und der Administration einer Django-App voraus. Eine vorhergehende Härtung des Betriebssystems wird empfohlen. Es wird keine Garantie für die Korrektheit dieser Anleitung gegeben.

Es ist sicher zu stellen, dass die Ports 80 und 443 offen, sowie `git`, `docker` und `docker-compose` installiert sind. Zusätzlich ist `docker` dergestalt einzurichten, dass es mit Hochfahren des Betriebssystems gestartet wird. Zur Installation wird ein Nutzer mit Sudo-Rechten, sowie eine Shell verwendet.

## Installation

Zuerst wird das Remote-Repository des Projekts geklont:

```
git clone https://codeberg.org/Fereon/lorenz-auftritt.git
```

Zur Vereinfachung der kommenden Eingaben wird der Ordner gewechselt.
```
cd lorenz-auftritt/Anwendung/
```

Die für das erstellen das Projekt notwendige Datei `.env.prod` ist in diesem Ordner anzulegen: (Zu Ergänzende Einträge werden mit `****` markiert.)
```
DEBUG=0
SECRET_KEY="****"
DJANGO_ALLOWED_HOSTS=****
ADMIN_URL=****
EMAIL_HOST=****
EMAIL_PORT=****
EMAIL_USER=****
EMAIL_PASSWORD=****

```

Mit `docker-compose` werden nun die Services erstellt.
```
sudo docker-compose -f docker-compose.prod.yml build
```

Da der Webserver (Nginx) ohne ein gültiges SSL-Zertifikat nicht starten kann, wird über die `standalone`-Option von `certbot` eines angefordert. Mit Ausführung des Befehls werden weitere Nutzereingaben benötigt.
```
sudo docker-compose -f docker-compose.prod.yml run -p 80:80 -p 443:443 --entrypoint certbot certbot certonly --standalone
```

Mithilfe von `docker-compose` wird das Projekt zur Einrichtung der Django-App vorläufig bereitgestellt.
``` 
sudo docker-compose -f docker-compose.prod.yml up -d
```

Zur Einrichtung der Django-App wird eine Shell um den Container gestartet. Es werden mit `collectstatic` die statischen Inhalte gesammelt, mit `migrate` die Datenbank initialisiert und mit `createsuperuser` ein Nutzer für das CMS eingerichtet.
```
sudo docker-compose -f docker-compose.prod.yml exec app /bin/ash
python manage.py collectstatic
python manage.py migrate
python manage.py createsuperuser
exit
```

Die Webseite kann nun aufgerufen werden. Alle statischen Inhalte sollten nun korrekt geladen und dargestellt werden. Mithilfe des CMS (Django-Admin) und des eingerichteten Nutzerkonto können die Domain für die Generierung der Sitemap eingestellt, Kontaktanfragen verwaltet, sowie Blogeinträge erstellt werden.

Um die Software mit Hochfahren des Betriebssystems starten zu lassen, werden die Container zunächst wieder heruntergefahren.
```
sudo docker-compose -f docker-compose.prod.yml down
```

Hiernach wird der vorberiete Systemd-Service in das entsprechende Verzeichnis kopiert, eingerichtet und gestartet.
```
sudo cp docker-compose-app.service /etc/systemd/system/docker-compose-app.service
sudo systemctl enable docker-compose-app.service
sudo systemctl start docker-compose-app.service
```

Optional können mit `cron` Aufgaben, wie das regelmäßige Bereinigen der Datenbank, der Logs und anfordern eines neuen SSL-Zertifikats geplant werden. Entsprechende Vorlagen sind im Unterordner `lorenz-auftritt/Anwendung/` zu finden.

Die Installation ist nun abgeschlossen.